﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using EloiExperiments.Toolbox.RandomTool;

[CustomEditor(typeof(RandomToolMono))]
[CanEditMultipleObjects]
public class RandomToolMonoEditor : Editor
{

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        RandomToolMono mono = (RandomToolMono)target;
        
        if (GUILayout.Button("Random Float: 100"))
        {
            mono.m_lastStringResult ="Float: "+ RandomTool.GetFloat(100, false);
        }
        if (GUILayout.Button("Random char: Hey mon ami !"))
        {
            mono.m_lastStringResult = "Char: " + RandomTool.GetRandomCharacter(new char[] { 'H', 'e', 'y', 'm', 'o', 'n', 'a', 'i' });
        }


    }

}