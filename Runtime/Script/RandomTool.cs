﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EloiExperiments.Toolbox.RandomTool
{
    public static class RandomTool
    {
        public static bool m_basedOnTime;
        public static DateTime m_choosedTime;

        static public float GetFloat(float value, bool useNegative = true) {
            return GetFloat(useNegative ? -value : 0, value);
        }
        static public float GetFloat(float min, float max) {
            if (m_basedOnTime) { return 0; }
            else {
                return UnityEngine.Random.Range(min, max);
            }
        }
        static public Vector3 GetVector3(float range = 1f) { return new Vector3(GetFloat(-1, 1), GetFloat(-1, 1), GetFloat(-1, 1)) * range; }
        static public Vector2 GetVector2(float range = 1f) { return new Vector2(GetFloat(-1, 1), GetFloat(-1, 1)) * range; }
        static public Quaternion GetQuaternion() { return Quaternion.Euler(new Vector3(GetFloat(-360, 360), GetFloat(-360, 360), GetFloat(-360, 360))); }
        static public Vector3 GetDirection() { return GetQuaternion() * Vector3.forward; }

        static public int GetInteger(int min, int max)
        {
            return Mathf.RoundToInt(GetFloat(min, max));
        }
        static public int GetInteger(int value, bool useNegative = true)
        {
            return Mathf.RoundToInt(GetFloat(value, useNegative));
        }
        static public int GetIntegerExclusive(int value, bool useNegative = true)
        {
            int min, max;
            if (useNegative)
            {
                min = value + 1;
                max = value - 1;
            }
            else {
                min = 0;
                max = value - 1;
            }

            return Mathf.Clamp( Mathf.RoundToInt(GetFloat(value, useNegative)), min, max);
        }

        static public char GetRandomCharacter(char[] selection)
        {
            return GetRandomInArray<char>(selection);
        }

        static public T GetRandomInArray<T>(T[] array)
        {
            return array[GetIntegerExclusive(array.Length, false)];
        }
      

        static public T GetRandomInArray<T>(List<T> list)
        {
            return list[GetIntegerExclusive(list.Count, false)];
        }


    }
}
